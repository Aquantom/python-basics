# Operators

a = 5 # '=' is assignment operator
# Left operand and Right operand

# Arithmetic operators
# + - / // * %
x = 5
y = 8
z = x + y
print(f"The value of z is {z}")

z = x / y
print(f"The value of z is {z}")

z = x // y
print(f"The value of z is {z}")

z = x * y
print(f"The value of z is {z}")

z = x ** y
print(f"The value of z is {z}")

x = 15
y = 8
z = x % y
print(f"The value of z is {z}")

z = (1+2)*3**2//2+3
print(f"The value of z is {z}")

z = 1+2*3**2//2+3
    # 1+2*9//2+3
    # 1+18//2+3
    # 1+9+3
    # 13
print(f"The value of z is {z}")

z = 10
z += 1 # In c/c++/java, z++(post-increment), ++z(pre-increment)
print(f"The value of z is {z}")

z -= 1
print(f"The value of z is {z}")

x = 19
z += x
print(f"The value of z is {z}")

z -= x
print(f"The value of z is {z}")

z *= x
print(f"The value of z is {z}")


z /= x
print(f"The value of z is {z}")


# Relation operators
x = 13
y = 5
z = x < y
print(f"The value of z is {z}")

z = (x <= y)
print(f"The value of z is {z}")


z = (x > y)
print(f"The value of z is {z}")

z = (x == y)
print(f"The value of z is {z}")


z = (x != y)
print(f"The value of z is {z}")

# Logical operators

x = True
y = False

z = x and y
print(f"The value of z is {z}")

x = True
y = True

z = x and y
print(f"The value of z is {z}")

x = False
y = True
z = x or y
print(f"The value of z is {z}")

z = not x
print(f"The value of z is {z}")
z = not y
print(f"The value of z is {z}")


# Bitwise operator
x = False
y = True
z = x ^ y
print(f"The value of z is {z}")

x = False
y = False
z = x ^ y
print(f"The value of z is {z}")

x = 3
y = 4
z = x|y
print(f"The value of z is {z}")

x = 3
y = 4
z = x&y
print(f"The value of z is {z}")

x = 3
y = 2
z = x<<y
print(f"The value of z is {z}")

x = 3
y = 2
z = x>>y
print(f"The value of z is {z}")

# Membership operators
xlist = [1, 2, 5, 6, 3, 78]
z = 9 in xlist
print(f"The value of z is {z}")

z = 9 not in xlist
print(f"The value of z is {z}")

# Identity operators
x = 5
y = 7
z = x is y
print(f"The value of z is {z}")

z = x is not y
print(f"The value of z is {z}")

